#!/usr/bin/env bash

use_tbl() {
    local TBL_PATH="$HOME/Documents/codeberg/tiny-bash-lib"
    if [ -z "$1" ]; then
        for file in $TBL_PATH/src/*.sh; do
            source "$file"
        done
    else
        for arg in "$@"; do
            if [[ -f "$TBL_PATH/src/$arg.sh" ]]; then
                source "$TBL_PATH/src/$arg.sh"
            else
                echo "[TBL] Warning: File '$TBL_PATH/src/$arg.sh' not found."
            fi
        done
    fi
}; export -f use_tbl

