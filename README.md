# Tiny-bash-library
Personal bash library providing functions for text color, style, logs plus some
utilities.

### Use
~/.bashrc
```bash
source "/path/to/tiny-bash-lib/use_tbl.sh"
```
