#!/usr/bin/env bash

# log_emerg [MESSAGE]
function log_emerg () {
    (set +u; printf "%b" "${LIBSTY_DIM}$(date +"%Y-%m-%d %H:%M:%S") ${LIBSTY_RST}${LIBCOL_RED}${LIBSTY_BOLD}${LIBSTY_REVERSE}[Emergency]${LIBSTY_RST}${LIBCOL_RST} $1${LIBSTY_RST}" >&2);
    echo "$1" | logger -p user.emerg
}

# log_alert [MESSAGE]
function log_alert () {
    (set +u; printf "%b" "${LIBSTY_DIM}$(date +"%Y-%m-%d %H:%M:%S") ${LIBSTY_RST}${LIBCOL_RED}${LIBSTY_BOLD}${LIBSTY_BLINK}[Alert]${LIBSTY_RST}${LIBCOL_RST} $1${LIBSTY_RST}" >&2);
    echo "$1" | logger -p user.alert
}

# log_critical [MESSAGE]
function log_critical () {
    (set +u; printf "%b" "${LIBSTY_DIM}$(date +"%Y-%m-%d %H:%M:%S") ${LIBSTY_RST}${LIBCOL_YELLOW}${LIBSTY_BOLD}${LIBSTY_REVERSE}[Critical]${LIBSTY_RST}${LIBCOL_RST} $1${LIBSTY_RST}" >&2);
    echo "$1" | logger -p user.crit
}

# log_error [MESSAGE]
function log_error () {
    (set +u; printf "%b" "${LIBSTY_DIM}$(date +"%Y-%m-%d %H:%M:%S") ${LIBSTY_RST}${LIBCOL_RED}${LIBSTY_BOLD}[Error]${LIBSTY_RST}${LIBCOL_RST} $1${LIBSTY_RST}" >&2);
    echo "$1" | logger -p user.error
}

# log_warning [MESSAGE]
function log_warning () {
    (set +u; printf "%b" "${LIBSTY_DIM}$(date +"%Y-%m-%d %H:%M:%S") ${LIBSTY_RST}${LIBCOL_YELLOW}${LIBSTY_BOLD}[Warning]${LIBSTY_RST}${LIBCOL_RST} $1${LIBSTY_RST}" >&2);
    echo "$1" | logger -p user.warning
}

# log_notice [MESSAGE]
function log_notice () {
    (set +u; printf "%b" "${LIBSTY_DIM}$(date +"%Y-%m-%d %H:%M:%S") ${LIBSTY_RST}${LIBCOL_GREEN}${LIBSTY_BOLD}[Notice]${LIBSTY_RST}${LIBCOL_RST} $1${LIBSTY_RST}" >&2);
    echo "$1" | logger -p user.notice
}

# log_info [MESSAGE]
function log_info () {
    (set +u; printf "%b" "${LIBSTY_DIM}$(date +"%Y-%m-%d %H:%M:%S") ${LIBSTY_RST}${LIBCOL_BLUE}${LIBSTY_BOLD}[Info]${LIBSTY_RST}${LIBCOL_RST} $1${LIBSTY_RST}" >&2);
    echo "$1" | logger -p user.info
}

# log_debug [MESSAGE]
function log_debug () {
    (set +u; printf "%b" "${LIBSTY_DIM}$(date +"%Y-%m-%d %H:%M:%S") ${LIBSTY_RST}${LIBCOL_MAGENTA}${LIBSTY_BOLD}[Debug]${LIBSTY_RST}${LIBCOL_RST} $1${LIBSTY_RST}" >&2);
    echo "$1" | logger -p user.debug
}

# Highlight a keyword

# log_emerg_hl [COLOR_CODE] [KEYWORD] [MESSAGE]
function log_emerg_hl () {
    (set +u; printf "%b" "${LIBSTY_DIM}$(date +"%Y-%m-%d %H:%M:%S") ${LIBSTY_RST}${LIBCOL_RED}${LIBSTY_BOLD}${LIBSTY_REVERSE}[Emergency]${LIBSTY_RST}${LIBCOL_RST} $3" | sed -E "s/\b$2\b/$(printf '%b' ${1}${LIBSTY_REVERSE}${2}${LIBSTY_RST}${LIBCOL_HLRST}${LIBCOL_RST})/" >&2);
    echo "$3" | logger -p user.emerg
}

# log_alert_hl [COLOR_CODE] [KEYWORD] [MESSAGE]
function log_alert_hl () {
    (set +u; printf "%b" "${LIBSTY_DIM}$(date +"%Y-%m-%d %H:%M:%S") ${LIBSTY_RST}${LIBCOL_RED}${LIBSTY_BOLD}${LIBSTY_BLINK}[Alert]${LIBSTY_RST}${LIBCOL_RST} $3" | sed -E "s/\b$2\b/$(printf '%b' ${1}${LIBSTY_REVERSE}${2}${LIBSTY_RST}${LIBCOL_HLRST}${LIBCOL_RST})/" >&2);
    echo "$3" | logger -p user.alert
}

# log_critical_hl [COLOR_CODE] [KEYWORD] [MESSAGE]
function log_critical_hl () {
    (set +u; printf "%b" "${LIBSTY_DIM}$(date +"%Y-%m-%d %H:%M:%S") ${LIBSTY_RST}${LIBCOL_YELLOW}${LIBSTY_BOLD}${LIBSTY_REVERSE}[Critical]${LIBSTY_RST}${LIBCOL_RST} $3" | sed -E "s/\b$2\b/$(printf '%b' ${1}${LIBSTY_REVERSE}${2}${LIBSTY_RST}${LIBCOL_HLRST}${LIBCOL_RST})/" >&2);
    echo "$3" | logger -p user.crit
}


# log_error_hl [COLOR_CODE] [KEYWORD] [MESSAGE]
function log_error_hl () {
    (set +u; printf "%b" "${LIBSTY_DIM}$(date +"%Y-%m-%d %H:%M:%S") ${LIBSTY_RST}${LIBCOL_RED}${LIBSTY_BOLD}[Error]${LIBSTY_RST}${LIBCOL_RST} $3" | sed -E "s/\b$2\b/$(printf '%b' ${1}${LIBSTY_REVERSE}${2}${LIBSTY_RST}${LIBCOL_HLRST}${LIBCOL_RST})/" >&2);
    echo "$3" | logger -p user.error
}

# log_warning_hl [COLOR_CODE] [KEYWORD] [MESSAGE]
function log_warning_hl () {
    (set +u; printf "%b" "${LIBSTY_DIM}$(date +"%Y-%m-%d %H:%M:%S") ${LIBSTY_RST}${LIBCOL_YELLOW}${LIBSTY_BOLD}[Warning]${LIBSTY_RST}${LIBCOL_RST} $3" | sed -E "s/\b$2\b/$(printf '%b' ${1}${LIBSTY_REVERSE}${2}${LIBSTY_RST}${LIBCOL_HLRST}${LIBCOL_RST})/" >&2);
    echo "$3" | logger -p user.warning
}

# log_notice_hl [COLOR_CODE] [KEYWORD] [MESSAGE]
function log_notice_hl () {
    (set +u; printf "%b" "${LIBSTY_DIM}$(date +"%Y-%m-%d %H:%M:%S") ${LIBSTY_RST}${LIBCOL_GREEN}${LIBSTY_BOLD}[Notice]${LIBSTY_RST}${LIBCOL_RST} $3" | sed -E "s/\b$2\b/$(printf '%b' ${1}${LIBSTY_REVERSE}${2}${LIBSTY_RST}${LIBCOL_HLRST}${LIBCOL_RST})/" >&2);
    echo "$3" | logger -p user.notice
}

# log_info_hl [COLOR_CODE] [KEYWORD] [MESSAGE]
function log_info_hl () {
    (set +u; printf "%b" "${LIBSTY_DIM}$(date +"%Y-%m-%d %H:%M:%S") ${LIBSTY_RST}${LIBCOL_BLUE}${LIBSTY_BOLD}[Info]${LIBSTY_RST}${LIBCOL_RST} $3" | sed -E "s/\b$2\b/$(printf '%b' ${1}${LIBSTY_REVERSE}${2}${LIBSTY_RST}${LIBCOL_HLRST}${LIBCOL_RST})/" >&2);
    echo "$3" | logger -p user.info
}

# log_debug_hl [COLOR_CODE] [KEYWORD] [MESSAGE]
function log_debug_hl () {
    (set +u; printf "%b" "${LIBSTY_DIM}$(date +"%Y-%m-%d %H:%M:%S") ${LIBSTY_RST}${LIBCOL_MAGENTA}${LIBSTY_BOLD}[Debug]${LIBSTY_RST}${LIBCOL_RST} $3" | sed -E "s/\b$2\b/$(printf '%b' ${1}${LIBSTY_REVERSE}${2}${LIBSTY_RST}${LIBCOL_HLRST}${LIBCOL_RST})/" >&2);
    echo "$3" | logger -p user.debug
}

