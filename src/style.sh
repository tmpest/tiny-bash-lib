#!/usr/bin/env bash

LIBSTY_RST='\033[0m'

LIBSTY_BOLD='\033[1m'
LIBSTY_DIM='\033[2m'
LIBSTY_ITALIC='\033[3m'
LIBSTY_UNDERLINE='\033[4m'
LIBSTY_BLINK='\033[5m'
LIBSTY_REVERSE='\033[7m'
LIBSTY_INVISIBLE='\033[8m'

# style_bold [STRING]
function style_bold () {
    printf "%b" "${LIBSTY_BOLD}${1}${LIBSTY_RST}"
}

# style_dim [STRING]
function style_dim () {
    printf "%b" "${LIBSTY_DIM}${1}${LIBSTY_RST}"
}

# style_italic [STRING]
function style_italic () {
    printf "%b" "${LIBSTY_ITALIC}${1}${LIBSTY_RST}"
}

# style_underline [STRING]
function style_underline () {
    printf "%b" "${LIBSTY_UNDERLINE}${1}${LIBSTY_RST}"
}

# style_blinking [STRING]
function style_blinking () {
    printf "%b" "${LIBSTY_BLINK}${1}${LIBSTY_RST}"
}

# style_reverse [STRING]
function style_reverse () {
    printf "%b" "${LIBSTY_REVERSE}${1}${LIBSTY_RST}"
}

# style_invisible [STRING]
function style_invisible () {
    printf "%b" "${LIBSTY_INVISIBLE}${1}${LIBSTY_RST}"
}

