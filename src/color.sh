#!/usr/bin/env bash

# Reset color
LIBCOL_RST='\033[39m'

LIBCOL_BLACK='\033[30m'
LIBCOL_RED='\033[31m'
LIBCOL_GREEN='\033[32m'
LIBCOL_YELLOW='\033[33m'
LIBCOL_BLUE='\033[34m'
LIBCOL_MAGENTA='\033[35m'
LIBCOL_CYAN='\033[36m'
LIBCOL_LGRAY='\033[37m'
LIBCOL_DGRAY='\033[90m'
LIBCOL_LRED='\033[91m'
LIBCOL_LGREEN='\033[92m'
LIBCOL_LYELLOW='\033[93m'
LIBCOL_LBLUE='\033[94m'
LIBCOL_LMAGENTA='\033[95m'
LIBCOL_LCYAN='\033[96m'
LIBCOL_WHITE='\033[97m'

# Reset Highlight color
LIBCOL_HLRST='\033[49m'

LIBCOL_HLBLACK='\033[40m'
LIBCOL_HLRED='\033[41m'
LIBCOL_HLGREEN='\033[42m'
LIBCOL_HLYELLOW='\033[43m'
LIBCOL_HLBLUE='\033[44m'
LIBCOL_HLMAGENTA='\033[45m'
LIBCOL_HLCYAN='\033[46m'
LIBCOL_HLLGRAY='\033[47m'
LIBCOL_HLDGRAY='\033[100m'
LIBCOL_HLLRED='\033[101m'
LIBCOL_HLLGREEN='\033[102m'
LIBCOL_HLLYELLOW='\033[103m'
LIBCOL_HLLBLUE='\033[104m'
LIBCOL_HLLMAGENTA='\033[105m'
LIBCOL_HLLCYAN='\033[106m'
LIBCOL_HLWHITE='\033[107m'

# color_black [STRING]
function color_black () {
    printf "%b" "${LIBCOL_BLACK}${1}${LIBCOL_RST}"
}

# color_red [STRING]
function color_red () {
    printf "%b" "${LIBCOL_RED}${1}${LIBCOL_RST}"
}

# color_green [STRING]
function color_green () {
    printf "%b" "${LIBCOL_GREEN}${1}${LIBCOL_RST}"
}

# color_yellow [STRING]
function color_yellow () {
    printf "%b" "${LIBCOL_YELLOW}${1}${LIBCOL_RST}"
}

# color_blue [STRING]
function color_blue () {
    printf "%b" "${LIBCOL_BLUE}${1}${LIBCOL_RST}"
}

# color_magenta [STRING]
function color_magenta () {
    printf "%b" "${LIBCOL_MAGENTA}${1}${LIBCOL_RST}"
}

# color_cyan [STRING]
function color_cyan () {
    printf "%b" "${LIBCOL_CYAN}${1}${LIBCOL_RST}"
}

# color_lgray [STRING]
function color_lgray () {
    printf "%b" "${LIBCOL_LGRAY}${1}${LIBCOL_RST}"
}

# color_dgray [STRING]
function color_dgray () {
    printf "%b" "${LIBCOL_DGRAY}${1}${LIBCOL_RST}"
}

# color_lred [STRING]
function color_lred () {
    printf "%b" "${LIBCOL_LRED}${1}${LIBCOL_RST}"
}

# color_lgreen [STRING]
function color_lgreen () {
    printf "%b" "${LIBCOL_LGREEN}${1}${LIBCOL_RST}"
}

# color_lyellow [STRING]
function color_lyellow () {
    printf "%b" "${LIBCOL_LYELLOW}${1}${LIBCOL_RST}"
}

# color_lblue [STRING]
function color_lblue () {
    printf "%b" "${LIBCOL_LBLUE}${1}${LIBCOL_RST}"
}

# color_lmagenta [STRING]
function color_lmagenta () {
    printf "%b" "${LIBCOL_LMAGENTA}${1}${LIBCOL_RST}"
}

# color_lcyan [STRING]
function color_lcyan () {
    printf "%b" "${LIBCOL_LCYAN}${1}${LIBCOL_RST}"
}

# color_white [STRING]
function color_white () {
    printf "%b" "${LIBCOL_WHITE}${1}${LIBCOL_RST}"
}

# color_hlblack [STRING]
function color_hlblack () {
    printf "%b" "${LIBCOL_HLBLACK}${1}${LIBCOL_HLRST}"
}

# color_hlred [STRING]
function color_hlred () {
    printf "%b" "${LIBCOL_HLRED}${1}${LIBCOL_HLRST}"
}

# color_hlgreen [STRING]
function color_hlgreen () {
    printf "%b" "${LIBCOL_HLGREEN}${1}${LIBCOL_HLRST}"
}

# color_hlyellow [STRING]
function color_hlyellow () {
    printf "%b" "${LIBCOL_HLYELLOW}${1}${LIBCOL_HLRST}"
}

# color_hlblue [STRING]
function color_hlblue () {
    printf "%b" "${LIBCOL_HLBLUE}${1}${LIBCOL_HLRST}"
}

# color_hlmagenta [STRING]
function color_hlmagenta () {
    printf "%b" "${LIBCOL_HLMAGENTA}${1}${LIBCOL_HLRST}"
}

# color_hlcyan [STRING]
function color_hlcyan () {
    printf "%b" "${LIBCOL_HLCYAN}${1}${LIBCOL_HLRST}"
}

# color_hllgray [STRING]
function color_hllgray () {
    printf "%b" "${LIBCOL_HLLGRAY}${1}${LIBCOL_HLRST}"
}

# color_hldgray [STRING]
function color_hldgray () {
    printf "%b" "${LIBCOL_HLDGRAY}${1}${LIBCOL_HLRST}"
}

# color_hllred [STRING]
function color_hllred () {
    printf "%b" "${LIBCOL_HLLRED}${1}${LIBCOL_HLRST}"
}

# color_hllgreen [STRING]
function color_hllgreen () {
    printf "%b" "${LIBCOL_HLLGREEN}${1}${LIBCOL_HLRST}"
}

# color_hllyellow [STRING]
function color_hllyellow () {
    printf "%b" "${LIBCOL_HLLYELLOW}${1}${LIBCOL_HLRST}"
}

# color_hllblue [STRING]
function color_hllblue () {
    printf "%b" "${LIBCOL_HLLBLUE}${1}${LIBCOL_HLRST}"
}

# color_hllmagenta [STRING]
function color_hllmagenta () {
    printf "%b" "${LIBCOL_HLLMAGENTA}${1}${LIBCOL_HLRST}"
}

# color_hllcyan [STRING]
function color_hllcyan () {
    printf "%b" "${LIBCOL_HLLCYAN}${1}${LIBCOL_HLRST}"
}

# color_hlwhite [STRING]
function color_hlwhite () {
    printf "%b" "${LIBCOL_HLWHITE}${1}${LIBCOL_HLRST}"
}
