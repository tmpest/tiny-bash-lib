#!/usr/bin/env bash

# util_list [ITEM 1] (ITEM 2) ..
function util_list () {
    for ITEM in "${@}"; do
        printf "%b" "$ITEM\n"
    done
}

# util_list_select [ITEM 1] (ITEM 2) ..
# @return selection
function util_list_select () {
    local INDEX=0
    for ITEM in "${@}"; do
        (set +u; printf "%b" "${LIBCOL_BLUE}${LIBSTY_DIM}${INDEX}.${LIBCOL_RST}${LIBSTY_RST} $ITEM\n");
        ((INDEX++))
    done
    read -rp "Selection (q to abort): " 2>&1

    if util_is_int "$REPLY" ; then
        return $REPLY
    else
        return -1
    fi
}

# util_exec_on_fail [COMMAND] ..
function util_exec_on_fail () {
    local STATUS=$? # Can be used in the command
    if [ $STATUS -ne 0 ]; then
        eval "$@"
    else
        return $STATUS
    fi
}

# util_exit_on_fail
function util_exit_on_fail () {
    local STATUS=$?
    if [ $STATUS -ne 0 ]; then
        exit $STATUS
    else
        return $STATUS
    fi
}

# util_exec_on [ERROR_CODE] [COMMAND] ..
function util_exec_on () {
    local STATUS=$? # Can be used in the command
    if [ $STATUS -eq $1 ]; then
        eval "$@"
    else
        return $STATUS
    fi
}

# util_exit_on [ERROR_CODE]
function util_exit_on () {
    local STATUS=$?
    if [ $STATUS -eq $1 ]; then
        exit $STATUS
    else
        return $STATUS
    fi
}

# util_prompt_user [MESSAGE]
# @return 0 success, 1 failure
function util_prompt_user () {
    read -p "$1"
    if [[ $REPLY =~ ^[Yy]$ ]]; then
        return 0
    else
        return 1
    fi
}

# util_clipboard
function util_clipboard () {
    if [ "$XDG_SESSION_TYPE" = "x11" ]; then
        xclip -selection clipboard -o
    elif [ "$XDG_SESSION_TYPE" = "wayland" ]; then
        wl-paste
    fi
}

# util_regex_match [PATTERN] [VALUE]
# @return 0 success, 1 failure
function util_regex_match () {
    if [[ $2 =~ $1 ]]; then
        return 0
    else
        return 1
    fi
}

# util_is_url [STRING]
# @return 0 success, 1 failure
function util_is_url () {
    local url_regex='(https?|ftp|file)://[-[:alnum:]\+&@#/%?=~_|!:,.;]+'
    util_regex_match "$url_regex" "$1"
}

# util_is_int [VALUE]
# @return 0 success, 1 failure
function util_is_int () {
    local int_regex='^-?[0-9]+$'
    util_regex_match "$int_regex" "$1"
}

# util_is_float [VALUE]
# @return 0 success, 1 failure
function util_is_float () {
    local float_regex='^[0-9]+([.][0-9]+)?$'
    util_regex_match "$float_regex" "$1"
}

# util_is_ipv4 [VALUE]
# @return 0 success, 1 failure
function util_is_ipv4 () {
    local ipv4_regex='^((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$'
    util_regex_match "$ipv4_regex" "$1"
}

# util_is_ipv6 [VALUE]
# @return 0 success, 1 failure
function util_is_ipv6 () {
    local ipv6_regex='^$|^[0-9a-fA-F]{1,4}:[0-9a-fA-F]{1,4}:[0-9a-fA-F]{1,4}:[0-9a-fA-F]{1,4}:[0-9a-fA-F]{1,4}:[0-9a-fA-F]{1,4}:[0-9a-fA-F]{1,4}:[0-9a-fA-F]{1,4}$'
    util_regex_match "$ipv6_regex" "$1"
}

# util_hyperlink [URL] [STRING]
function util_hyperlink () {
   printf '%b' "\e]8;;${1}\a${2}\e]8;;\a"
}

# util_print_inplace [MESSAGE]
function util_print_inplace () {
    printf "\r%s" "${1}"
}

